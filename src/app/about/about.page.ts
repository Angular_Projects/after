import { Component, OnInit } from '@angular/core';
import { AlertController, Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  subscription: any;
  aboutSteps: any;

  constructor(public storage: Storage, private screenOrientation: ScreenOrientation, private alertCtrl: AlertController, private platform: Platform, public router: Router) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      // navigator['tabs/tabs/habit'].exitApp();
      this.router.navigate(['tabs/tabs/habit']);
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  resetTour = () => {
    this.storage.set('habitTourComplete', false);
    this.storage.set('homeTourComplete', false);
  }

  clearStorageClick = () => {
    this.clearStorageAlert('Would you like to start the challenge from the beginning?');
  }

  clearStorage = () => {
    this.storage.clear();
    this.router.navigate(['intro']).then(() => {
      window.location.reload();
    });
  }

  async clearStorageAlert(data) {
    const prompt = await this.alertCtrl.create({
      message: data,
      cssClass: 'alertCustomCss',
      buttons: [
        {
          text: 'cancel',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            this.clearStorage();
          }
        }
      ]
    });

    await prompt.present();
  }

  toggleDetails(data) {
    if (data.showDetails) {
      data.showDetails = false;
      data.icon = 'add';
    } else {
      data.showDetails = true;
      data.icon = 'remove';
    }
  }

}
