import { Component, OnInit } from '@angular/core';
import { ChatService } from '../../providers/chat-service';
import { Storage } from '@ionic/storage';
import { Router, NavigationExtras } from '@angular/router';
import { AlertController, Platform, ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import * as introJs from 'intro.js/intro.js';
import { LocalNotifications } from '@ionic-native/local-notifications/ngx';
import { PopoverController } from '@ionic/angular';
import { Events } from '../../providers/events';
import { ModalPage } from '../modal/modal.page';
import isYesterday from 'date-fns/isYesterday'
import differenceInSeconds from 'date-fns/differenceInSeconds'
import { ToastController } from '@ionic/angular';
import { format } from 'date-fns';

import { JournalPage } from '../journal/journal.page';

@Component({
  selector: 'app-habit',
  templateUrl: './habit.page.html',
  styleUrls: ['./habit.page.scss'],
})

export class HabitPage implements OnInit {
  subscription: any;
  customSteps: any;
  habitArray: any[] = [];
  iWill: any;
  afterI: any;
  mydate: any;
  habitForm: FormGroup;
  habitInitial = true;
  viewMore = false;
  habitAdded = false;
  backEnabled = false;
  habitData: any;
  checked = false;
  afterIDisable = false;
  habitTotal: any;
  habitStreak: any;
  lastHabitDate: any;
  createHabitDate: any;
  result = isYesterday(new Date(2021, 2, 16));


  constructor(private localNotifications: LocalNotifications, public toastController: ToastController, public events: Events, public modalController: ModalController, public popoverController: PopoverController, private alertCtrl: AlertController,
    public router: Router, private formBuilder: FormBuilder, public chatService: ChatService, public storage: Storage, private platform: Platform) {
    this.habitForm = this.formBuilder.group({
      iWill: ['', Validators.required],
      afterI: [''],
      mydate: ['']
    });

    events.subscribe('backPressed', () => {
    });

    events.subscribe('habitAdded', () => {
      this.storage.get('habitData').then((data) => {
        console.log('event sub', data);
        this.habitAdded = true;
        const array = new Array(data);
        if (array[0]) {
          array.forEach((items) => {
            this.habitData = [];
            for (const i of items) {
              this.habitData?.push(i);
            }
          });
        }
      });
    });

    events.subscribe('habitdeleted', () => {
      this.storage.get('habitData').then((data) => {
        this.habitAdded = true;
        const array = new Array(data);
        if (array[0]) {
          array.forEach((items) => {
            this.habitData = [];
            for (const i of items) {
              this.habitData?.push(i);
            }
          });
        }
      });
    });
  }

  ngOnInit() {
  }

  ionViewWillEnter() {
    // this.storage.get('habitTourComplete').then((val) => {
    //   this.habitTourComplete = val;

    //   if (!this.habitTourComplete) {
    //     document.getElementById('scroll-container').scrollIntoView();
    //     introJs(document.querySelector('app-habit')).setOptions({
    //       steps: [
    //         {
    //           intro: 'Begin tour to learn more about The Paradime!'
    //         },
    //         {
    //           element: '#step3',
    //           intro: 'Your goal progress will be displayed on the calender icons with a tick mark. Based on the days completed, level will be improved.'
    //         },
    //         {
    //           element: '#step4',
    //           intro: 'This button is to mark your goal as complete. Corresponding day will be updated with a tick mark.'
    //         },
    //         {
    //           intro: 'Below section contains the tasks for the selected challenge.'
    //         }
    //       ],
    //       scrollToElement: false,
    //       exitOnOverlayClick: false,
    //       showStepNumbers: false,
    //       showProgress: true,
    //       skipLabel: 'Skip',
    //       doneLabel: 'Done',
    //       nextLabel: 'Next',
    //       prevLabel: 'Back'
    //     }).start()
    //       .oncomplete(() => {
    //         this.storage.set('habitTourComplete', true);
    //       });
    //   }
    // });


  }

  ionViewDidEnter() {

    this.checkTimeDelayandUncheck();
    console.log(this.result);
    // document.getElementById('scroll-container').scrollIntoView();
    this.storage.get('habitData').then((val) => {
      if (val) {
        this.habitInitial = false;
        this.habitAdded = true;
        this.habitData = val;
      }
    });

    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.habitInitial = false;
      this.habitAdded = false;
    });
  }

  ionViewWillLeave() {
    introJs().exit()
    this.subscription.unsubscribe();
  }

  start = () => {
    this.backEnabled = true;
    this.habitInitial = false;
    this.habitAdded = false;
  }


  goBack = () => {
    this.habitInitial = false;
    this.habitAdded = true;
    this.backEnabled = false;
  }


  addHabit = () => {

    this.habitInitial = false;
    this.habitAdded = true;
    this.backEnabled = false;

    const data = {
      formData: this.habitForm.value,
      checked: this.checked,
      habitTotal: 0,
      habitStreak: 0,
      lastHabitDate: new Date(),
      createHabitDate: new Date()
    }

    this.storage.get('habitData').then((val) => {
      if (val) {


        console.log(data.formData);
        if (data.formData.mydate) {
          console.log();
          data.formData.afterI = "get the reminder at " + format(new Date(data.formData.mydate), 'hh:mm');
        }


        val.push(data);
        this.storage.set('habitData', val).then(() => {
          this.habitAdded = true;
          this.events.publish('habitAdded');
          this.afterI = '';
          this.iWill = '';
        });
      }
      else {
        if (data.formData) {
          this.storage.set('habitData', Array(data)).then(() => {
            this.habitAdded = true;
            this.events.publish('habitAdded');
            this.afterI = '';
            this.iWill = '';
          });
        }
      }
    });
  }


  // Traverse through the entire array and update the checked flag


  checkTimeDelayandUncheck = () => {

    this.storage.get('habitData').then((data) => {
      const array = new Array(data);
      if (array[0]) {
        array.forEach((items) => {
          this.habitData = [];
          for (const i of items) {
            this.habitData?.push(i);
            console.log(i.lastHabitDate + " and " + (differenceInSeconds(new Date(), i.lastHabitDate)));
            // if (isYesterday(i.lastHabitDate === true)) {
            if (differenceInSeconds(new Date(), i.lastHabitDate) > 70) {    // 1 minute day
              i.checked = false;
            }
          }
          console.log(this.habitData);
          this.storage.set('habitData', this.habitData);
        });
      }
    });

    // Use isYesterday for Prod. Difference in Minutes only for testing
    // As we cannot wait 1 day, we check if diff is 1 minute for streak and uncheck
    // Difference in day wont work as its not right
  }

  habitCompleted = (index, event) => {
    event.preventDefault();
    event.stopPropagation();

    this.habitData[index].checked = event.currentTarget.checked;

    // this.habitData[index].habitTotal = this.habitData[index].habitTotal + 1;
    this.storage.set('habitData', this.habitData);
    if (event.currentTarget.checked) {
      this.openModal(index);
    }
    /*
    this.storage.get('habitData').then((val) => {
      if (val) {
        this.habitTotal = val[index].habitTotal;
        this.habitStreak = val[index].habitStreak;
        console.log(val[index].habitStreak + ' ' + index + ' ' + val[index].habitTotal);
      }
    });
    */
  }

  deleteHabit = (event, i) => {
    event.preventDefault();
    event.stopPropagation();

    this.habitArray = this.habitData.filter((item, index) => i !== index);
    this.storage.set('habitData', this.habitArray).then(() => this.events.publish('habitdeleted'));
  }

  viewAllHabits = () => {
    this.router.navigate(['habit-list']);
  }

  afterIchange = (event, val) => {
    event.preventDefault();
    event.stopPropagation();
    event.nativeEvent.stopImmediatePropagation();

    this.mydate = "00:00";
  }

  setRemainder = (hoursVal) => {
    const triggerDate: any = new Date(hoursVal);
    this.afterIDisable = (triggerDate.getHours() > 0 || triggerDate.getMinutes() > 0) ? true : false;

    this.localNotifications.schedule([
      {
        id: 1,
        title: 'The Paradime - How are you feeling today? ',
        text: 'Track how you feel by entering your mood in journals.',
        // trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 12)) },
        led: 'FF0000',
        trigger: {
          every: {
            hour: triggerDate.getHours() > 0 ? triggerDate.getHours() : null,
            minute: triggerDate.getMinutes() > 0 ? triggerDate.getMinutes() : null
          }
        },
        smallIcon: 'res://app_notification',
        icon: 'res://icon.png'
      },
      {
        id: 2,
        title: 'The Paradime - Chapter 2 is unlocked!',
        text: 'Continue reading the next chapter - `The Green eyed monster`',
        trigger: { at: new Date(new Date().getTime() + (1000 * 60 * 60 * 24)) },
        led: 'FF0000',
        smallIcon: 'res://app_notification',
        icon: 'res://icon.png'
      }

    ]);
  }

  async openModal(index) {
    const myModal = await this.modalController.create({
      component: JournalPage,
      componentProps: {
        todoIndex: index
      },
      cssClass: 'modal-css',
      backdropDismiss: true
    });

    myModal.onDidDismiss().then(() => {
      this.presentToast();
    });

    return await myModal.present();
  }

  async presentToast() {
    const toast = await this.toastController.create({
      message: 'Your record has been saved.',
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }
}
