import { Component, OnInit, Input } from '@angular/core';
import { ChatService, FeedData } from '../../providers/chat-service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';
import differenceInSeconds from 'date-fns/differenceInSeconds'
import { Platform } from '@ionic/angular';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-journal',
  templateUrl: './journal.page.html',
  styleUrls: ['./journal.page.scss'],
})
export class JournalPage implements OnInit {
  journalStatement: any;
  journalStatementArray = [];
  feedData: FeedData[] = [];
  i: any;
  j: any;
  emoticon: any;
  items: any;
  challenges: any;
  journalSubmitDisable = true;
  iconStatus: any;
  journalFeedData = [];
  journalButton = false;
  firstName: any;
  totaldays: any;
  subscription: any;
  habitData: any = [];
  iconChecked1 = false;
  iconChecked2 = false;
  iconChecked3 = false;
  iconChecked4 = false;
  iconChecked5 = false;
  journalDate: any;
  messageOne: any;
  messageTwo: any;
  countsBronze: any;
  countsRuby: any;
  countsSilver: any;
  countsDiamond: any;
  countsGold: any;

  @Input() todoIndex: any;

  constructor(public chatService: ChatService, public storage: Storage, public modalController: ModalController, private platform: Platform, private screenOrientation: ScreenOrientation, public router: Router,) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.storage.get('todayDateDB').then((val) => {
      this.totaldays = val;
    });
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.items = [
      {
        id: 1,
        name: 'First',
        img: 'toy'
      },
      {
        id: 2,
        name: 'Second',
        img: 'confetti'
      },
      {
        id: 3,
        name: 'Third',
        img: 'jazz'
      },
      {
        id: 4,
        name: 'Fourth',
        img: 'bachelor-party'
      },
      {
        id: 5,
        name: 'Fifth',
        img: 'jazz'
      }
    ];
  }

  ionViewDidEnter() {
    this.storage.get('firstName').then((val) => {
      this.firstName = val;
    });

    this.storage.get('feedData').then((data) => {
      const array = new Array(data);

      if (array[0]) {
        array.forEach((items) => {
          this.feedData = [];
          for (const i of items) {
            this.feedData.push(i);
          }
        });
      }

      if (this.feedData.length >= 2) {
        this.journalButton = true;
      }
    });



    this.storage.get('habitData').then((val) => {
      if (val) {
        this.habitData = val;
        this.talktoUserModal(this.habitData);
      }
    });


    this.storage.get('achievementsData').then((val) => {
      if (val) {
        this.countsBronze = val.BronzeData;
        this.countsRuby = val.RubyData;
        this.countsSilver = val.SilverData;
        this.countsDiamond = val.DiamondData;
        this.countsGold = val.GoldData;
      }
    });




    this.subscription = this.platform.backButton.subscribeWithPriority(0, () => {
      this.router.navigate(['tabs/tabs/habit']);
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  removeColoredIcon = () => {
    this.items.forEach((item) => {
      item.img = item.img.replace('Colored', '');
    });
  }

  emoticonClick = (item, index) => {
    if (item.name === 'First') {
      if (item.img === 'toy') {
        this.items[index].img = 'toy';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
        this.iconChecked1 = !this.iconChecked1;
        this.iconChecked2 = false;
        this.iconChecked3 = false;
        this.iconChecked4 = false;
        this.iconChecked5 = false;
      }
    }
    else if (item.name === 'Second') {
      if (item.img === 'confetti') {
        this.items[index].img = 'confetti';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
        this.iconChecked2 = !this.iconChecked2;
        this.iconChecked1 = false;
        this.iconChecked3 = false;
        this.iconChecked4 = false;
        this.iconChecked5 = false;
      }
    }
    else if (item.name === 'Third') {
      if (item.img === 'jazz') {
        this.items[index].img = 'jazz';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
        this.iconChecked3 = !this.iconChecked3;
        this.iconChecked1 = false;
        this.iconChecked2 = false;
        this.iconChecked4 = false;
        this.iconChecked5 = false;
      }
    }
    else if (item.name === 'Fourth') {
      if (item.img === 'bachelor-party') {
        this.items[index].img = 'bachelor-party';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
        this.iconChecked4 = !this.iconChecked4;
        this.iconChecked1 = false;
        this.iconChecked3 = false;
        this.iconChecked2 = false;
        this.iconChecked5 = false;
      }

    }
    else if (item.name === 'Fifth') {
      if (item.img === 'jazz') {
        this.items[index].img = 'jazz';
        this.journalSubmitDisable = false;
        this.emoticon = item.img;
        this.iconStatus = item.name;
        this.iconChecked5 = !this.iconChecked5;
        this.iconChecked1 = false;
        this.iconChecked3 = false;
        this.iconChecked4 = false;
        this.iconChecked2 = false;
      }
    }
  }

  talktoUserModal = (habitItem) => {

    console.log("Talk to modal :" + JSON.stringify(habitItem[this.todoIndex]));

    switch (habitItem[this.todoIndex].habitTotal) {
      case 0:
        this.messageOne = "Hey, good job for starting the habit! What do you think? Can you make it successful";
        this.messageTwo = "Write few words to appreciate yourself!";
        break;
      case 1:
        this.messageOne = "Good to see you back! And you did it again! Good job! ";
        this.messageTwo = "Don't worry it is too small! Write what you think.";
        break;
      case 2:
        this.messageOne = "Congrats! You have earned a Bronze badge for completing 3 days! ";
        this.messageTwo = "Write some nice words about yourself here.";
        break;
      case 4:
        this.messageOne = "Completed 5 days! Now it looks like this is start of something great! ";
        this.messageTwo = "I hope you are seeing how the starting trouble was removed! What you think? ";
        break;
      case 5:
        this.messageOne = "Completed 5 days! Now it looks like this is start of something great! ";
        this.messageTwo = "I hope you are seeing how easy it is start a task! What do you think? ";
        break;
      case 8:
        this.messageOne = "You are doing a wonderful job! Keep in mind every single successful day is important! ";
        this.messageTwo = "I believe most of the days you feel like doing more even after tiny task, Is it?";
        break;
      case 9:
        this.messageOne = "Congratulations! You have earned a Silver badge for completing 10 days! ";
        this.messageTwo = "Write a few words about how great you are!";
        break;
      case 12:
        this.messageOne = "Almost here for 2 weeks, don't worry if you are not seeing changes, but it is happening!";
        this.messageTwo = "Do you think it's time to introduce changes or increase the size of habit? If not fine. ";
        break;
      case 20:
        this.messageOne = "Woah!!! Congratulations for successfully completing 21 days with this habit. ";
        this.messageTwo = "Kudos to all your persistance and determination! You deserve to write how you succeeded!";
        break;
      case 25:
        this.messageOne = "You have done it!!! It looks to me like can do anything now. You are almost close to become a master!";
        this.messageTwo = "Are you finding this useful or you want to change few things? Feel free to increase habit size.";
        break;
      case 49:
        this.messageOne = "50 days of successful tiny habits! Now it feels you are in total control of this habit! You are a conqueror!";
        this.messageTwo = "It's time for the world to know what worked and what not worked? Please share with us and let us know!";
        break;
      default:
        this.messageOne = "Task done! Select one of the below to mark how you feel now.";
        this.messageTwo = "Write a few words to appreciate yourself. ";
        if (habitItem[this.todoIndex].lastHabitDate) {
          //     if (isYesterday(this.habitData[this.todoIndex].lastHabitDate)) {
          if ((differenceInSeconds(new Date(), habitItem.lastHabitDate) <= 120)) {
            if (habitItem[this.todoIndex].habitStreak === 6) {
              this.messageOne = "Congratulations! You have earned a Ruby badge for successfully completing this habit with 7 days streak! ";
              this.messageTwo = "If you can do it for a week, you can do it again and again! What do you say are you in? ";
            }
          }
          else if (habitItem[this.todoIndex].habitStreak === 13) {
            this.messageOne = "Congratulations! You have earned a Diamond badge for successfully completing this habit with 14 days streak! ";
            this.messageTwo = "You did it! Although we suggest not to worry too much about streaks or magical numbers, it is still very cool to go for 21 days! What do you say? ";
          }

        }
        break;
    }

    if (habitItem[this.todoIndex].habitTotal === 0) {
      this.messageOne = "Hey, good job for starting the habit! What do you think? Can you make it successful";
      this.messageTwo = "Write few words to appreciate yourself!";
    }
  }


  updateAchievements = (habitItem) => {
    if (habitItem.habitStreak === 7) {
      this.storage.set('achievementsData', {
        BronzeData: this.countsBronze,
        RubyData: this.countsRuby + 1,
        SilverData: this.countsSilver,
        DiamondData: this.countsDiamond,
        GoldData: this.countsGold
      });
    }
    else if (habitItem.habitStreak === 14) {
      this.storage.set('achievementsData', {
        BronzeData: this.countsBronze,
        RubyData: this.countsRuby,
        SilverData: this.countsSilver,
        DiamondData: this.countsDiamond + 1,
        GoldData: this.countsGold
      });
    }

    if (habitItem.habitTotal === 3) {
      this.storage.set('achievementsData', {
        BronzeData: this.countsBronze + 1,
        RubyData: this.countsRuby,
        SilverData: this.countsSilver,
        DiamondData: this.countsDiamond,
        GoldData: this.countsGold
      });
    }
    else if (habitItem.habitTotal === 10) {
      this.storage.set('achievementsData', {
        BronzeData: this.countsBronze,
        RubyData: this.countsRuby,
        SilverData: this.countsSilver + 1,
        DiamondData: this.countsDiamond,
        GoldData: this.countsGold
      });
    }
    else if (habitItem.habitTotal === 21) {
      this.storage.set('achievementsData', {
        BronzeData: this.countsBronze,
        RubyData: this.countsRuby,
        SilverData: this.countsSilver,
        DiamondData: this.countsDiamond,
        GoldData: this.countsGold + 1
      });
    }
  }


  addJournal = () => {
    this.journalSubmitDisable = true;

    const journalObj = {
      journal: (this.journalStatement && this.journalStatement.length > 0 && this.journalStatement !== undefined) ? this.journalStatement : `Feeling ${this.iconStatus}`,
      emoticon: this.emoticon,
      userName: this.firstName,
      day: this.totaldays,
      habitData: this.habitData[this.todoIndex].formData,
      journalDate: new Date()
    };

    const array = new Array(journalObj);

    array.forEach((item) => {
      this.journalStatementArray.push(item);
    });

    this.items.forEach((item, index) => {
      item.img = item.img.replace('Colored', '');
    });

    this.storage.get('habitData').then((val) => {
      if (val) {
        this.habitData = val;
        this.habitData[this.todoIndex].habitTotal = this.habitData[this.todoIndex].habitTotal + 1;

        if (this.habitData[this.todoIndex].lastHabitDate) {
          //     if (isYesterday(this.habitData[this.todoIndex].lastHabitDate)) {
          if ((differenceInSeconds(new Date(), this.habitData[this.todoIndex].lastHabitDate) <= 120)) {

            console.log(new Date());
            console.log(this.habitData[this.todoIndex].lastHabitDate);
            console.log(differenceInSeconds(new Date(), this.habitData[this.todoIndex].lastHabitDate));
            this.habitData[this.todoIndex].habitStreak = this.habitData[this.todoIndex].habitStreak + 1;
          }
          else {
            this.habitData[this.todoIndex].habitStreak = 0;
          }

        }
        this.updateAchievements(this.habitData[this.todoIndex]);
        this.habitData[this.todoIndex].lastHabitDate = new Date();
        this.storage.set('habitData', this.habitData);
      }
    });

    this.storage.set('journalAddition', this.journalStatementArray);

    setTimeout(() => {
      this.storage.get('journalAddition').then((data) => {
        this.feedData.unshift(data[data.length - 1]);
        this.storage.set('feedData', this.feedData);
      });
    }, 0);

    this.journalStatement = [];
    this.modalController.dismiss();
  }

  viewAllJournals = () => {
    this.router.navigate(['journals']);
  }


}
