import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HabitListPage } from './habit-list.page';

describe('HabitListPage', () => {
  let component: HabitListPage;
  let fixture: ComponentFixture<HabitListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HabitListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HabitListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
