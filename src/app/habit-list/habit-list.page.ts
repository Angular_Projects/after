import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import differenceInSeconds from 'date-fns/differenceInSeconds';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import getDate from 'date-fns/getDate';
import getMonth from 'date-fns/getMonth';
import getISODay from 'date-fns/getISODay';


@Component({
  selector: 'app-habit-list',
  templateUrl: './habit-list.page.html',
  styleUrls: ['./habit-list.page.scss'],
})
export class HabitListPage implements OnInit {
  habitData: any = [];
  subscription: any;
  recordData: any = [];
  feedData: any = [];
  partfeedData: any = [];
  habitPlanned: any;
  habitAdded: any;
  habitTotal: any;
  habitProg: any;
  countsBronze: any;
  countsRuby: any;
  countsSilver: any;
  countsDiamond: any;
  countsGold: any;
  journalfeedDataDate: any;
  journalfeedDataMonth: any;
  journalfeedDataWeek: any;




  constructor(private storage: Storage, private platform: Platform, public router: Router) {


    this.storage.get('feedData').then((data) => {
      const array = new Array(data);
      for (let i = 0; i < 3; i++) {
        if (data[i]) {
          this.partfeedData.push(data[i]);
          data[i].journalfeedDataDate = getDate(data[i].journalDate);

          switch (getISODay(data[i].journalDate)) {
            case 1:
              data[i].journalfeedDataWeek = "Monday";
              break;
            case 2:
              data[i].journalfeedDataWeek = "Tuesday";
              break;
            case 3:
              data[i].journalfeedDataWeek = "Wednesday";
              break;
            case 4:
              data[i].journalfeedDataWeek = "Thursday";
              break;
            case 5:
              data[i].journalfeedDataWeek = "Friday";
              break;
            case 6:
              data[i].journalfeedDataWeek = "Saturday";
              break;
            case 7:
              data[i].journalfeedDataWeek = "Sunday";
              break;
            default:
              data[i].journalfeedDataWeek = "Journal";
              break;
          }


          switch (getMonth(data[i].journalDate)) {
            case 0:
              data[i].journalfeedDataMonth = "January";
              break;
            case 2:
              data[i].journalfeedDataMonth = "February";
              break;
            case 3:
              data[i].journalfeedDataMonth = "March";
              break;
            case 4:
              data[i].journalfeedDataMonth = "April";
              break;
            case 5:
              data[i].journalfeedDataMonth = "May";
              break;
            case 6:
              data[i].journalfeedDataMonth = "June";
              break;
            case 7:
              data[i].journalfeedDataMonth = "July";
              break;
            case 8:
              data[i].journalfeedDataMonth = "August";
              break;
            case 9:
              data[i].journalfeedDataMonth = "September";
              break;
            case 10:
              data[i].journalfeedDataMonth = "October";
              break;
            case 11:
              data[i].journalfeedDataMonth = "November";
              break;
            case 12:
              data[i].journalfeedDataMonth = "December";
              break;
            default:
              data[i].journalfeedDataMonth = "Note";
              break;
          }

        }
      }

    });

  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.storage.get('achievementsData').then((val) => {
      if (val) {
        this.countsBronze = val.BronzeData;
        this.countsRuby = val.RubyData;
        this.countsSilver = val.SilverData;
        this.countsDiamond = val.DiamondData;
        this.countsGold = val.GoldData;
      }
    });

    this.storage.get('recordInput').then((data) => {
      if (data) {
        this.recordData = data;
      }
    });

    this.storage.get('habitData').then((data) => {
      console.log('event sub', data);
      this.habitAdded = true;
      const array = new Array(data);
      if (array[0]) {
        array.forEach((items) => {
          this.habitData = [];
          for (const i of items) {
            this.habitData?.push(i);
            i.habitPlanned = Math.round((differenceInSeconds(new Date(), i.createHabitDate)) / 70);
            if (i.habitPlanned != 0) {
              i.habitProg = (Math.round(i.habitTotal / i.habitPlanned * 100));
            }
            else {
              i.habitProg = 0;
            }

            if (!i.habitProg) {
              i.habitProg = 0;
            }
          }
        });
      }
    });

    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.router.navigate(['/tabs/tabs/habit']);
    });
  }

  habitListBackClick = () => {
    this.router.navigate(['/tabs/tabs/habit']);
  }

  viewAllJournals = () => {
    this.router.navigate(['journals']);
  }


}
