import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';
import getDate from 'date-fns/getDate';
import getMonth from 'date-fns/getMonth';
import getISODay from 'date-fns/getISODay';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-journals',
  templateUrl: './journals.page.html',
  styleUrls: ['./journals.page.scss'],
})
export class JournalsPage implements OnInit {
  feedData = [];
  subscription: any;
  habitItem = [];
  partfeedData = [];
  journalfeedDataDate: any;
  journalfeedDataMonth: any;
  journalfeedDataWeek: any;
  items = [];       // infinite scroll
  numTimesLeft = 1; // infinite scroll
  readItems = 0;    // infinite scroll

  constructor(private storage: Storage, private platform: Platform, public router: Router, private screenOrientation: ScreenOrientation) {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
    this.addMoreItems();   // infinite scroll
  }

  loadData(event) {
    setTimeout(() => {

      console.log('Done');
      this.addMoreItems();
      event.target.complete();


    }, 500);
  }

  addMoreItems() {
    this.storage.get('feedData').then((data) => {
      const array = new Array(data);
      console.log(data[0]);
      console.log("Calling Entire array length" + this.partfeedData);
      for (let i = this.readItems; i < (this.readItems + 10); i++) {
        if (data[i]) {
          this.partfeedData.push(data[i]);
          data[i].journalfeedDataDate = getDate(data[i].journalDate);

          switch (getISODay(data[i].journalDate)) {
            case 1:
              data[i].journalfeedDataWeek = "Mon";
              break;
            case 2:
              data[i].journalfeedDataWeek = "Tue";
              break;
            case 3:
              data[i].journalfeedDataWeek = "Wed";
              break;
            case 4:
              data[i].journalfeedDataWeek = "Thu";
              break;
            case 5:
              data[i].journalfeedDataWeek = "Fri";
              break;
            case 6:
              data[i].journalfeedDataWeek = "Sat";
              break;
            case 7:
              data[i].journalfeedDataWeek = "Sun";
              break;
            default:
              data[i].journalfeedDataWeek = "Journal";
              break;
          }


          switch (getMonth(data[i].journalDate)) {
            case 0:
              data[i].journalfeedDataMonth = "Jan";
              break;
            case 2:
              data[i].journalfeedDataMonth = "Feb";
              break;
            case 3:
              data[i].journalfeedDataMonth = "Mar";
              break;
            case 4:
              data[i].journalfeedDataMonth = "Apr";
              break;
            case 5:
              data[i].journalfeedDataMonth = "May";
              break;
            case 6:
              data[i].journalfeedDataMonth = "Jun";
              break;
            case 7:
              data[i].journalfeedDataMonth = "Jul";
              break;
            case 8:
              data[i].journalfeedDataMonth = "Aug";
              break;
            case 9:
              data[i].journalfeedDataMonth = "Sep";
              break;
            case 10:
              data[i].journalfeedDataMonth = "Oct";
              break;
            case 11:
              data[i].journalfeedDataMonth = "Nov";
              break;
            case 12:
              data[i].journalfeedDataMonth = "Dec";
              break;
            default:
              data[i].journalfeedDataMonth = "Note";
              break;
          }
        }
      }

      this.readItems = this.readItems + 10;

      if (this.partfeedData.length === data.length) {
        console.log("Fill " + this.partfeedData);
        this.numTimesLeft = 0;
      }
    });
  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
  }

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.router.navigate(['/tabs/tabs/home']);
    });
  }

  journalBackClick = () => {
    this.router.navigate(['/tabs/tabs/home']);
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }
}
