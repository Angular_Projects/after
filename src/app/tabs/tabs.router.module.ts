import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: 'tabs',
    component: TabsPage,
    children: [
      {
        path: 'habit',
        children: [
          {
            path: '',
            loadChildren: () => import('../habit/habit.module').then(m => m.HabitPageModule)
          }
        ]
      },
      {
        path: 'home',
        children: [
          // {
          //   path: '',
          //   loadChildren: () => import('../home/home.module').then(m => m.HomePageModule)
          // }
          {
            path: '',
            loadChildren: () => import('../habit-list/habit-list.module').then(m => m.HabitListPageModule)
          }
        ]
      },
      {
        path: 'journal',
        children: [
          {
            path: '',
            loadChildren: () => import('../journal/journal.module').then(m => m.JournalPageModule)
          }
        ]
      },
      {
        path: 'about',
        children: [
          {
            path: '',
            loadChildren: () => import('../about/about.module').then(m => m.AboutPageModule)
          }
        ]
      }
    ]
  },
  {
    path: '',
    redirectTo: '/tabs/tabs/home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class TabsPageRoutingModule { }
