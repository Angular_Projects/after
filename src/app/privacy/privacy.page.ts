import { Component, OnInit } from '@angular/core';
import { Platform } from '@ionic/angular';
import { Router } from '@angular/router';

@Component({
  selector: 'app-privacy',
  templateUrl: './privacy.page.html',
  styleUrls: ['./privacy.page.scss'],
})
export class PrivacyPage implements OnInit {
  subscription: any;

  constructor(private platform: Platform, public router: Router) { }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.subscription = this.platform.backButton.subscribeWithPriority(1, () => {
      this.router.navigate(['tabs/tabs/about']);
    });
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  privacyBackClick = () => {
    this.router.navigate(['tabs/tabs/about']);
  }

}
