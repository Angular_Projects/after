import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ModalController, IonSlides } from '@ionic/angular';
import { ScreenOrientation } from '@ionic-native/screen-orientation/ngx';

@Component({
  selector: 'app-intro',
  templateUrl: './intro.page.html',
  styleUrls: ['./intro.page.scss'],
})
export class IntroPage implements OnInit {
  slideOpts = {
    slidesPerView: 1,
    spaceBetween: 10,
    centeredSlides: true
  };

  slideContents: any;
  isSelected: boolean;

  @ViewChild('slides', { static: false }) slides: IonSlides;

  constructor(private storage: Storage, private screenOrientation: ScreenOrientation, public router: Router, public modalController: ModalController) {
    this.storage.set('todayDateDB', 1);
    this.storage.remove('fill');  // Replay story
    this.storage.set('achievementsData', {
      BronzeData: 0,
      RubyData: 0,
      SilverData: 0,
      DiamondData: 0,
      GoldData: 0
    });

    // this.storage.set('darkToggleValue', true);

  }

  ngOnInit() {
    this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);

    this.slideContents = [
      {
        header: 'CHOOSE A GOAL',
        image: 'archery',
        description: 'Find a long term goal and decide a habit which you want to introduce into your life to reach the goal. '
      },
      {
        header: 'MAKE IT TINY',
        image: 'atom',
        description: 'Make is embarrassingly small that it is requires no effort or pain and takes just 5 to 30 seconds.'
      },
      {
        header: 'FIND A CUE',
        image: 'ball-pool',
        description: 'Decide when do you need to do this habit. Do it after an existing routine or do it after a certain time '
      },
      {
        header: 'FELL GOOD',
        image: 'party',
        description: 'Take some time to do a little celebration and Write 3 words to express your positive feeling '
      },
      {
        header: 'REPEAT',
        image: 'repeat',
        description: 'There is no step 5. That`s it!  Just repeat steps 2 to 4 everyday. You will get there.  '
      }
    ]
  }

  slideChanged = (slides) => {
    this.slides.getActiveIndex().then(index => {
      if (index === 4) {
        const a = Array.from(document.getElementsByClassName('swiper-pagination-bullets') as HTMLCollectionOf<HTMLElement>)
        a[0].style.display = 'none';
      }
      else {
        const a = Array.from(document.getElementsByClassName('swiper-pagination-bullets') as HTMLCollectionOf<HTMLElement>)
        a[0].style.display = 'block';
      }
    });
  }

  goToTabs = () => {
    this.router.navigate(['/tabs/tabs/habit']);
  }
}
