import { Component, Input } from '@angular/core';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-modal',
  templateUrl: './modal.page.html',
  styleUrls: ['./modal.page.scss'],
})
export class ModalPage {
  // Data passed in by componentProps
  @Input() firstAction: string;
  @Input() secondAction: string;
  @Input() thirdAction: string;
  @Input() audioParam: any;

  modalTapHeading = true;
  recordInput: string;
  recordArray: any = [];
  recordForm: FormGroup;

  constructor(public router: Router, public modalController: ModalController, private formBuilder: FormBuilder, private storage: Storage) {
    this.recordForm = this.formBuilder.group({
      recordInput: ['', Validators.required]
    });
  }

  addRecord = (event) => {
    event.preventDefault();
    event.stopPropagation();

    this.storage.get('recordInput').then((val) => {
      if (val) {
        val.push(this.recordForm.value)
        this.storage.set('recordInput', val);
        this.modalController.dismiss();
      }
      else {
        this.storage.set('recordInput', Array(this.recordForm.value));
        this.modalController.dismiss();
      }
    });
  }
}
