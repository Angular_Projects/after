import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
import { Events } from '../providers/events';

export class FeedData {
  journal: any;
  emoticon: any;
  userName: any;
  date: any;
  day: number;
}

@Injectable()
export class ChatService {
  posts: any;
  constructor(private http: HttpClient, private events: Events, public alertCtrl: AlertController) {
  }

  async notifyPlayer(data) {
    const prompt = await this.alertCtrl.create({
      message: data,
      cssClass: 'alertCustomCss',
      buttons: [
        {
          text: 'OK'
        },
      ]
    });

    await prompt.present();
  }
}
